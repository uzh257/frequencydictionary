package word_count_dict

import (
	"errors"
	"sort"
)

type word_count struct {
	Word  string
	Count int
}

type Words struct {
	Words []word_count
}

func (words *Words) MultipleAdd(source []string) {
	for _, word := range source {
		words.Add(word)
	}
}

func (words *Words) Add(word string) {
	position, err := words.wordNumber(word)
	if err != nil {
		words.addNewWord(word)
	} else {
		words.incrementWord(position)
	}
}

func (words *Words) wordNumber(word string) (int, error) {
	for i, existingWord := range words.Words {
		if existingWord.Word != word {
			continue
		}

		return i, nil
	}

	return 0, errors.New("no word")
}

func (words *Words) addNewWord(word string) {
	new_word := word_count{Count: 1, Word: word}
	words.Words = append(words.Words, new_word)
	words.sort()
}

func (words *Words) incrementWord(position int) {
	words.Words[position].Count++
	words.sort()
}

func (words *Words) sort() {
	sort.Slice(words.Words, func(i, j int) bool {
		return words.Words[i].Count < words.Words[j].Count
	})
}
