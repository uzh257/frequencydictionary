module gitlab.com/uzh257/frequencydictionary

go 1.17

require (
	github.com/Grey-Fox/gofb2 v0.1.1
	github.com/VladimirMarkelov/fb2text v0.0.0-20190131040324-9be8135c4d78
)

require (
	github.com/huandu/xstrings v1.3.2 // indirect
	golang.org/x/net v0.0.0-20220105145211-5b0dc2dfae98 // indirect
	golang.org/x/text v0.3.6 // indirect
)
