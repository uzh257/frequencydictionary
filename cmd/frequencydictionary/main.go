package main

import (
	"fmt"
	"github.com/VladimirMarkelov/fb2text"
	"gitlab.com/uzh257/frequencydictionary/internal/word_count_dict"
	"regexp"
	"strings"
	"time"
)

func main() {
	start := time.Now()
	dstName := "test.fb2"

	_, lines := fb2text.ParseBook(dstName, true)
	lines = fb2text.FormatBook(lines, 100, false)

	dict := handle(lines)

	for _, word := range dict.Words {
		fmt.Printf("%s - %d\n", word.Word, word.Count)
	}

	duration := time.Since(start)
	fmt.Println(duration)
}

func handle(lines []string) word_count_dict.Words {
	var dict word_count_dict.Words

	for _, line := range lines {
		iterateOneLine(line, &dict)
	}

	return dict
}

func iterateOneLine(line string, dict *word_count_dict.Words) {
	line = strings.ToLower(line)
	m1 := regexp.MustCompile(`[^а-яА-Яa-zA-z\s]`)
	line = m1.ReplaceAllString(line, " ")
	words := strings.Fields(line)
	for _, word := range words {
		iterateOneWord(word, dict)
	}
}

func iterateOneWord(word string, dict *word_count_dict.Words) {
	valid := []string{}
	trimmedWord := strings.Trim(word, " ")
	if len([]rune(trimmedWord)) > 3 {
		valid = append(valid, trimmedWord)
	}

	dict.MultipleAdd(valid)
}
